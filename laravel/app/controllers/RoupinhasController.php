<?php

use \Roupinhas;

class RoupinhasController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.roupinhas.index')->with('imagens', Roupinhas::orderBy('ordem', 'ASC')->paginate(24));
	}

	public function mostrar($id){
		return View::make('frontend.fancy')->with('imagem', Roupinhas::find($id))->with('dir', 'roupinhas');
	}
}
