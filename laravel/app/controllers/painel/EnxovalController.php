<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Enxoval;

class EnxovalController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.enxoval.index')->with('registros', Enxoval::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.enxoval.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Enxoval;

		$t=Date('YmdHis');
		$imagem = Thumb::make('imagem', 1600, null, 'enxoval/', $t);
		Thumb::make('imagem', 90, 90, 'enxoval/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;

		$object->legenda = Input::get('legenda');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Foto inserida com sucesso.');
			return Redirect::route('painel.enxoval.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Foto!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.enxoval.edit')->with('registro', Enxoval::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Enxoval::find($id);

		$t=Date('YmdHis');
		$imagem = Thumb::make('imagem', 1600, null, 'enxoval/', $t);
		Thumb::make('imagem', 90, 90, 'enxoval/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;

		$object->legenda = Input::get('legenda');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Foto alterada com sucesso.');
			return Redirect::route('painel.enxoval.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Foto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Enxoval::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Foto removida com sucesso.');

		return Redirect::route('painel.enxoval.index');
	}

}