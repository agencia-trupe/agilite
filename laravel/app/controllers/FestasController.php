<?php

use \Festas;

class FestasController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.festas.index')->with('imagens', Festas::orderBy('ordem', 'ASC')->paginate(24));
	}

	public function mostrar($id){
		return View::make('frontend.fancy')->with('imagem', Festas::find($id))->with('dir', 'festas');
	}
}
