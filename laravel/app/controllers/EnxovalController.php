<?php

use \Enxoval;

class EnxovalController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.enxoval.index')->with('imagens', Enxoval::orderBy('ordem', 'ASC')->paginate(24));
	}

	public function mostrar($id){
		return View::make('frontend.fancy')->with('imagem', Enxoval::find($id))->with('dir', 'enxoval');
	}
}
