<?php

use \Locacao;

class LocacaoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.locacao.index')->with('imagens', Locacao::orderBy('ordem', 'ASC')->paginate(24));
	}

	public function mostrar($id){
		return View::make('frontend.fancy')->with('imagem', Locacao::find($id))->with('dir', 'locacao');
	}
}
