<?php

use \Empresa;

class EmpresaController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.empresa.index')->with('texto', Empresa::first());
	}

}
