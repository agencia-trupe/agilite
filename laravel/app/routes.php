<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home.alt', 'uses' => 'HomeController@index'));
Route::get('nossas-pecas', array('as' => 'festas', 'uses' => 'FestasController@index'));
Route::get('nossas-pecas/imagem/{id}', array('as' => 'festas.show', 'uses' => 'FestasController@mostrar'));
Route::get('venda-e-locacao', array('as' => 'locacao', 'uses' => 'LocacaoController@index'));
Route::get('venda-e-locacao/imagem/{id}', array('as' => 'locacao.show', 'uses' => 'LocacaoController@mostrar'));
Route::get('oficinas', array('as' => 'enxoval', 'uses' => 'EnxovalController@index'));
Route::get('oficinas/imagem/{id}', array('as' => 'enxoval.show', 'uses' => 'EnxovalController@mostrar'));
Route::get('presentes-e-lembrancas', array('as' => 'roupinhas', 'uses' => 'RoupinhasController@index'));
Route::get('presentes-e-lembrancas/imagem/{id}', array('as' => 'roupinhas.show', 'uses' => 'RoupinhasController@mostrar'));
Route::get('empresa', array('as' => 'empresa', 'uses' => 'EmpresaController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('contato', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('empresa', 'Painel\EmpresaController');
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('festas', 'Painel\FestasController');
	Route::resource('locacao', 'Painel\LocacaoController');
	Route::resource('enxoval', 'Painel\EnxovalController');
	Route::resource('roupinhas', 'Painel\RoupinhasController');
//NOVASROTASDOPAINEL//
});