@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.contato.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}" required>
				</div>
				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<textarea name="endereco" class="form-control" id="inputEndereço" >{{$registro->endereco }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" name="facebook" class="form-control" id="inputFacebook" value="{{$registro->facebook}}">
				</div>	
				<div class="form-group">
					<label for="inputInstagram">Instagram</label>
					<input type="text" name="instagram" class="form-control" id="inputInstagram" value="{{$registro->instagram}}">
				</div>	
				<div class="form-group">
					<label for="inputPinterest">Pinterest</label>
					<input type="text" name="pinterest" class="form-control" id="inputPinterest" value="{{$registro->pinterest}}">
				</div>	

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop