<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=980,initial-scale=1">
    
    <meta name="keywords" content="" />

	<title>Espaço Petit</title>
	<meta name="description" content="">
	<meta property="og:title" content="Agilité"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Agilité"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="assets/images/layout/espacoPetit-facebook.png"/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/pure/grids-core-min',
		'vendor/pure/grids-units-min',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	<!--[if lte IE 8]>
        <link rel="stylesheet" href="{{asset('assets/vendor/pure/grids-responsive-old-ie-min.css')}}">
    <![endif]-->
    <!--[if gt IE 8]><!-->
    	<link rel="stylesheet" href="{{asset('assets/vendor/pure/grids-responsive-min.css')}}">        
    <!--<![endif]-->

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('js/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/modernizr'))?>
	@endif

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-54518615-1', 'auto');
	  ga('send', 'pageview');

	</script>	
</head>
<body>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
      	
      	<div class="nuvem nuvem-1"></div>
        <div class="nuvem nuvem-2"></div>
        <div class="nuvem nuvem-3"></div>
        <div class="nuvem nuvem-4"></div>
        <div class="nuvem nuvem-5"></div>
        <div class="nuvem nuvem-6"></div>

        @if(str_is('home*', Route::currentRouteName()))

            <div class="centralizar hide-desktop template">
                <div class="pure-g">  
                    <div class="pure-u-1 pure-u-md-1-3">
                        <div class="pure-pad-0">
                            
                            <div class="links-social">
		                    	@if($contato->facebook)
		                        	<a href="{{$contato->facebook}}" title="Facebook" target="_blank"><img src="assets/images/layout/icone-facebook.png" alt="Facebook"></a>
		                        @endif
		                        @if($contato->pinterest)
		                        	<a href="{{$contato->pinterest}}" title="Pinterest" target="_blank"><img src="assets/images/layout/icone-pinterest.png" alt="Pinterest"></a>
								@endif
		                        @if($contato->instagram)
		                        	<a href="{{$contato->instagram}}" title="Instagram" target="_blank"><img src="assets/images/layout/icone-instagram.png" alt="Instagram"></a>
		                        @endif
		                    </div>

                            <a href="" title="Mostrar Menu" class="btn-abrir-menu hide-tablet hide-desktop">menu</a>
                            
                            <nav>
                            	<ul>
                            		<li><a href="home" title="Página Inicial" @if(str_is('home*', Route::currentRouteName())) class="ativo" @endif><span>home</span></a></li>
			                        <li><a href="nossas-pecas" title="Nossas Peças" @if(str_is('festas*', Route::currentRouteName())) class="ativo" @endif><span>nossas peças</span></a></li>
			                        <li><a href="venda-e-locacao" title="Venda e Locação" @if(str_is('locacao*', Route::currentRouteName())) class="ativo" @endif><span>venda e locação</span></a></li>
			                        <li><a href="oficinas" title="Oficinas" @if(str_is('enxoval*', Route::currentRouteName())) class="ativo" @endif><span>oficinas</a></li>
			                        <li><a href="presentes-e-lembrancas" title="Presentes e Lembranças" @if(str_is('roupinhas*', Route::currentRouteName())) class="ativo" @endif><span>presentes e lembranças</span></a></li>
			                        <li><a href="empresa" title="Empresa" @if(str_is('empresa*', Route::currentRouteName())) class="ativo" @endif><span>empresa</span></a></li>
			                        <li><a href="contato" title="Contato" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif><span>contato</span></a></li>
                            	</ul>
                            </nav>
                        </div>
                    </div>

                    <div class="pure-u-1 pure-u-md-2-3">
                        <section class="pure-pad-0">
                            @yield('conteudo')
                        </section>
                    </div>

                </div>
            </div>

            <div id="home-desktop" class="hide-mobile hide-tablet">

                <div class="centralizar">

                    <div class="links-social">
                    	@if($contato->facebook)
                        	<a href="{{$contato->facebook}}" title="Facebook" target="_blank"><img src="assets/images/layout/icone-facebook.png" alt="Facebook"></a>
                        @endif
                        @if($contato->pinterest)
                        	<a href="{{$contato->pinterest}}" title="Pinterest" target="_blank"><img src="assets/images/layout/icone-pinterest.png" alt="Pinterest"></a>
						@endif
                        @if($contato->instagram)
                        	<a href="{{$contato->instagram}}" title="Instagram" target="_blank"><img src="assets/images/layout/icone-instagram.png" alt="Instagram"></a>
                        @endif
                    </div>

                    <ul id="navegacao-desktop">
                        <li id="li-home"><a href="home" title="Página Inicial">home</a></li>
                        <li id="li-festas"><a href="nossas-pecas" title="Nossas Peças">nossas peças</a></li>
                        <li id="li-locacao"><a href="venda-e-locacao" title="Venda e Locação">venda e locação</a></li>
                        <li id="li-enxoval"><a href="oficinas" title="Oficinas">oficinas</a></li>
                        <li id="li-roupinhas"><a href="presentes-e-lembrancas" title="Presentes e Lembranças">presentes e lembranças</a></li>
                        <li id="li-empresa"><a href="empresa" title="Empresa">empresa</a></li>
                        <li id="li-contato"><a href="contato" title="Contato">contato</a></li>
                    </ul>

                    <div id="mensagem-home">
                    	@yield('conteudo')
                    </div>
                    <div id="mensagem-home-back"></div>

                </div>
            </div>

        @else
        
            <div class="centralizar template">
                <div class="pure-g">  
                    <div class="pure-u-1 pure-u-md-1-3">
                        <div class="pure-pad-0">
                            <div class="links-social">
		                    	@if($contato->facebook)
		                        	<a href="{{$contato->facebook}}" title="Facebook" target="_blank"><img src="assets/images/layout/icone-facebook.png" alt="Facebook"></a>
		                        @endif
		                        @if($contato->pinterest)
		                        	<a href="{{$contato->pinterest}}" title="Pinterest" target="_blank"><img src="assets/images/layout/icone-pinterest.png" alt="Pinterest"></a>
								@endif
		                        @if($contato->instagram)
		                        	<a href="{{$contato->instagram}}" title="Instagram" target="_blank"><img src="assets/images/layout/icone-instagram.png" alt="Instagram"></a>
		                        @endif
		                    </div>

                            <a href="" title="Mostrar Menu" class="btn-abrir-menu hide-tablet hide-desktop">menu</a>
                            
                            <nav>
                            	<ul>
                            		<li><a href="home" title="Página Inicial" @if(str_is('home*', Route::currentRouteName())) class="ativo" @endif><span>home</span></a></li>
			                        <li><a href="nossas-pecas" title="Nossas Peças" @if(str_is('festas*', Route::currentRouteName())) class="ativo" @endif><span>nossas peças</span></a></li>
			                        <li><a href="venda-e-locacao" title="Venda e Locação" @if(str_is('locacao*', Route::currentRouteName())) class="ativo" @endif><span>venda e locação</span></a></li>
			                        <li><a href="oficinas" title="Oficinas" @if(str_is('enxoval*', Route::currentRouteName())) class="ativo" @endif><span>oficinas</span></a></li>
			                        <li><a href="presentes-e-lembrancas" title="Presentes e Lembranças" @if(str_is('roupinhas*', Route::currentRouteName())) class="ativo" @endif><span>presentes e lembranças</span></a></li>
			                        <li><a href="empresa" title="Empresa" @if(str_is('empresa*', Route::currentRouteName())) class="ativo" @endif><span>empresa</span></a></li>
			                        <li><a href="contato" title="Contato" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif><span>contato</span></a></li>
                            	</ul>
                            </nav>
                        </div>
                    </div>

                    <div class="pure-u-1 pure-u-md-2-3">
                        <section class="pure-pad-0">
                            @yield('conteudo')
                        </section>
                    </div>

                </div>
            </div>
        
        @endif
	
	</div>
	
	@if(!str_is('home*', Route::currentRouteName()))
		<div id="mundo-footer" class="hide-mobile"></div>
	@endif

	<footer>
		<div class="centralizar">
			<div class="pure-g">
				<div class="pure-u-1 pure-u-md-13-24">
					@if($contato->telefone)
						{{$contato->telefone}}
					@endif
					@if($contato->endereco)
						<div class="endereco">
							{{$contato->endereco}}
						</div>
					@endif
				</div>
				<div class="pure-u-1 pure-u-md-6-24">
					&copy; {{Date('Y')}} Agilité Produções<br>
					Todos os direitos reservados
				</div>
				<div class="pure-u-1 pure-u-md-5-24 link">
					<a href="http://www.trupe.net" target="_blank" title="Criação de Sites">Criação de Sites:<br>Trupe Agência Criativa<br><img src="assets/images/layout/trupe.png" alt="Criação de Sites : Trupe Agência Criativa"></a>
				</div>
			</div>
		</div>
	</footer>

	<?=Assets::JS(array(
		'vendor/fancybox/source/jquery.fancybox',
		'js/main'
	))?>

</body>
</html>
