@section('conteudo')

	<img src="assets/images/layout/logo-espaço-petit.png" alt="Espaço Petit">

	<div class="listaImagens">
		@if(sizeof($imagens))
				@foreach($imagens as $v)
					<a href="oficinas/imagem/{{$v->id}}" rel="galeria" title="{{$v->legenda}}" class="thumb">
						<div class="pure-border">
							<img src="assets/images/enxoval/thumbs/{{$v->imagem}}" alt="{{$v->legenda}}">
						</div>
					</a>
				@endforeach
				{{$imagens->links()}}
		@else
			<p>Nenhuma imagem encontrada</p>
		@endif
	</div>

@stop
