@section('conteudo')
	
	<img src="assets/images/layout/logo-espaço-petit.png" alt="Espaço Petit">

	<p>Contato</p>

	<form action="" method="post">
		@if(Session::has('enviado'))
			<div class="success">Sua mensagem foi enviada com sucesso!</div>
		@endif
		<input type="text" name="nome" placeholder="Nome" required id="input-nome">
		<input type="email" name="email" placeholder="E-mail" required id="input-email">
		<input type="text" name="telefone" placeholder="Telefone">
		<textarea name="mensagem" placeholder="Mensagem" required id="input-mensagem"></textarea>
		<input type="submit" value="enviar">
		@if($contato->telefone)
			<div class="telefone">
				{{$contato->telefone}}
			</div>
		@endif
	</form>


@stop
