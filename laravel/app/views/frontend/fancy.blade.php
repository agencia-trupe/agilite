<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=980,initial-scale=1">
    
    <meta name="keywords" content="" />

	<title>Espaço Petit</title>
	<meta name="description" content="">
	<meta property="og:title" content="Agilité"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Agilité"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="assets/images/layout/espacoPetit-facebook.png"/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'css/fancy'
	))?>
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('js/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/modernizr'))?>
	@endif

	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		// Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-', '');
		// ga('send', 'pageview');
	</script>
</head>
<body>
<div id="fb-root"></div>

	@if($imagem)
		<img src="assets/images/{{$dir}}/{{$imagem->imagem}}" alt="{{$imagem->legenda}}" style="max-height: 90%;display: block;margin: 0 auto;">
		<p>{{$imagem->legenda}}</p>
		
		<iframe src="//www.facebook.com/plugins/like.php?href={{ Request::url() }}&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=80&amp;appId=248486392025885" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:25px;" allowTransparency="true"></iframe>
	@endif
	
</body>
</html>
