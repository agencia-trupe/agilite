@section('conteudo')

	<img src="assets/images/layout/logo-espaço-petit.png" alt="Espaço Petit">
    <p>
    	Uma forma diferente de sentir...<br>
    	Transformando problemas em soluções,<br>
    	ideias em oportunidades.
    </p>

    <div class="ilustracao hide-desktop">
    	<img src="assets/images/layout/mundo-espacopetiti.png" alt="Espaço Petit">
    </div>

@stop
