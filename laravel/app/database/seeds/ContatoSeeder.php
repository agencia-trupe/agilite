<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'telefone' => '0000-0000',
				'endereco' => 'Endereco'
            )
        );

        DB::table('contato')->insert($data);
    }

}
