<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contato', function(Blueprint $table)
		{
			$table->string('facebook')->after('endereco');
			$table->string('pinterest')->after('facebook');
			$table->string('instagram')->after('pinterest');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contato', function(Blueprint $table)
		{
			$table->dropColumn('facebook');
			$table->dropColumn('pinterest');
			$table->dropColumn('instagram');
		});
	}

}
