$('document').ready( function(){

	$('.btn-abrir-menu').click( function(e){
		e.preventDefault();
		$('nav ul').toggleClass('aberto');
	});

	$('.thumb').fancybox({
		type : 'ajax',
		autoWidth : true,
		autoHeight : true,
		maxWidth: 800,
		helpers:  {
	        title:  null,
	        overlay : {
	        	css : { 
	        		'background' : 'rgba(227,195,107,.7)'
	        	}
	        }
	    }
	});

	$('form').submit( function(e){
		if($('#input-nome').val() == '' || $('#input-nome').val() == $('#input-nome').attr('placeholder')){
			alert('Informe seu nome!');
			e.preventDefault();
			return false;
		}
		if($('#input-email').val() == '' || $('#input-email').val() == $('#input-email').attr('placeholder')){
			alert('Informe seu e-mail!');
			e.preventDefault();
			return false;
		}
		if($('#input-mensagem').val() == '' || $('#input-mensagem').val() == $('#input-mensagem').attr('placeholder')){
			alert('Informe sua mensagem!');
			e.preventDefault();
			return false;
		}
	});
});